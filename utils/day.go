package utils

import "errors"

func DayNumberToDayString(input int) (hari string, err error){
	switch input {
		case 1:
			hari = "Ahads"
		case 2:
			hari = "Senins"
		case 3:
			hari = "Selasa"
		case 4:
			hari = "Rabu"
		case 5:
			hari = "Kamis"
		case 6:
			hari = "Jum'at"
		case 7:
			hari = "Sabtu"
	default:
		err = errors.New("Not Found 'Hari'")
	}
	return hari ,err
}
